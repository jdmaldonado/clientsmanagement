USE [master]
GO
/****** Object:  Database [ClientsManagement]    Script Date: 21/04/2016 1:41:22 p. m. ******/
CREATE DATABASE [ClientsManagement]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ClientsManagement', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.JDSQLINSTANCE\MSSQL\DATA\ClientsManagement.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ClientsManagement_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.JDSQLINSTANCE\MSSQL\DATA\ClientsManagement_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ClientsManagement] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ClientsManagement].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ClientsManagement] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ClientsManagement] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ClientsManagement] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ClientsManagement] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ClientsManagement] SET ARITHABORT OFF 
GO
ALTER DATABASE [ClientsManagement] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ClientsManagement] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ClientsManagement] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ClientsManagement] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ClientsManagement] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ClientsManagement] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ClientsManagement] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ClientsManagement] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ClientsManagement] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ClientsManagement] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ClientsManagement] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ClientsManagement] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ClientsManagement] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ClientsManagement] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ClientsManagement] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ClientsManagement] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ClientsManagement] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ClientsManagement] SET RECOVERY FULL 
GO
ALTER DATABASE [ClientsManagement] SET  MULTI_USER 
GO
ALTER DATABASE [ClientsManagement] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ClientsManagement] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ClientsManagement] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ClientsManagement] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ClientsManagement] SET DELAYED_DURABILITY = DISABLED 
GO
USE [ClientsManagement]
GO
/****** Object:  Table [dbo].[Ciudades]    Script Date: 21/04/2016 1:41:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ciudades](
	[IdCiudad] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdCiudad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 21/04/2016 1:41:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[IdCliente] [int] IDENTITY(1,1) NOT NULL,
	[Cedula] [nvarchar](20) NOT NULL,
	[Nombres] [nvarchar](100) NOT NULL,
	[Apellidos] [nvarchar](100) NOT NULL,
	[FechaNacimiento] [datetime] NULL,
	[Genero] [nvarchar](1) NOT NULL,
	[IdPais] [int] NOT NULL,
	[IdDepartamento] [int] NOT NULL,
	[IdCiudad] [int] NOT NULL,
	[TipoDireccion] [nvarchar](50) NULL,
	[Direccion] [nvarchar](100) NULL,
	[Telefono] [nvarchar](50) NULL,
	[Celular] [nvarchar](50) NULL,
	[EstadoCivil] [nvarchar](50) NOT NULL,
	[Hijos] [bit] NOT NULL,
	[Email] [nvarchar](100) NULL,
	[Twitter] [nvarchar](100) NULL,
	[Facebook] [nvarchar](100) NULL,
	[HoraInicio] [nvarchar](50) NULL,
	[HoraFin] [nvarchar](50) NULL,
	[Emisoras] [nvarchar](max) NULL,
	[RecibirCorreo] [bit] NOT NULL DEFAULT ('false'),
	[RecibirSms] [bit] NOT NULL DEFAULT ('false'),
	[Acepta] [bit] NOT NULL DEFAULT ('false'),
	[IdDigitador] [int] NOT NULL,
	[Estado] [bit] NOT NULL DEFAULT ('false'),
	[LlamadoPor] [int] NULL,
	[FechaLLamada] [datetime] NULL,
	[DuracionLlamada] [bigint] NULL,
	[EstadoLlamada] [smallint] NULL,
	[FechaCreacion] [datetime] NULL DEFAULT (getdate()),
	[Barrio] [nvarchar](50) NULL,
	[Observacion] [nvarchar](max) NULL,
	[IdEmpresa] [int] NULL,
	[IdOcupacion] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Departamentos]    Script Date: 21/04/2016 1:41:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Departamentos](
	[IdDepartamento] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdDepartamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Empresas]    Script Date: 21/04/2016 1:41:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empresas](
	[IdEmpresa] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdEmpresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ocupaciones]    Script Date: 21/04/2016 1:41:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ocupaciones](
	[IdOcupacion] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdOcupacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Operadores]    Script Date: 21/04/2016 1:41:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operadores](
	[IdOperador] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [bigint] NOT NULL,
	[Nombres] [nvarchar](50) NOT NULL,
	[Apellidos] [nvarchar](50) NOT NULL,
	[IdPerfil] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdOperador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Paises]    Script Date: 21/04/2016 1:41:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Paises](
	[IdPais] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdPais] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Perfiles]    Script Date: 21/04/2016 1:41:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Perfiles](
	[IdPerfil] [int] IDENTITY(1,1) NOT NULL,
	[Perfil] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdPerfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PerfilesRoles]    Script Date: 21/04/2016 1:41:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PerfilesRoles](
	[IdPerfil] [int] NOT NULL,
	[IdRol] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdPerfil] ASC,
	[IdRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 21/04/2016 1:41:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[IdRol] [int] IDENTITY(1,1) NOT NULL,
	[Rol] [nvarchar](50) NOT NULL,
	[Icono] [nvarchar](50) NOT NULL,
	[Href] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Ciudades] ON 

INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (1, N'Bogotá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (2, N'Puerto Nariño')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (3, N'Leticia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (4, N'Medellín')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (5, N'Abejorral')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (6, N'Abriaqui')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (7, N'Alejandría')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (8, N'Amagá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (9, N'Amalfi')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (10, N'Andes')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (11, N'Angelópolis')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (12, N'Angostura')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (13, N'Anorí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (14, N'Antioquia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (15, N'Anzá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (16, N'Apartadó')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (17, N'Arboletes')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (18, N'Argelia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (19, N'Armenia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (20, N'Barbosa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (21, N'Belmira')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (22, N'Bello')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (23, N'Betania')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (24, N'Betulia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (25, N'Bolívar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (26, N'Briseño')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (27, N'Buriticá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (28, N'Cáceres')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (29, N'Caicedo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (30, N'Caldas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (31, N'Campamento')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (32, N'Cañasgordas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (33, N'Caracolí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (34, N'Caramanta')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (35, N'Carepa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (36, N'Carmen de Viboral')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (37, N'Carolina')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (38, N'Caucasia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (39, N'Chigorodó')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (40, N'Cisneros')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (41, N'Cocorná')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (42, N'Concepción')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (43, N'Concordia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (44, N'Copacabana')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (45, N'Dabeiba')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (46, N'Don Matías')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (47, N'Ebéjico')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (48, N'El Bagre')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (49, N'Entrerríos')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (50, N'Envigado')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (51, N'Fredonia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (52, N'Frontino')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (53, N'Giraldo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (54, N'Girardota')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (55, N'Gómez Plata')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (56, N'Granada')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (57, N'Guadalupe')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (58, N'Guarne')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (59, N'Guatapé')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (60, N'Heliconia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (61, N'Hispania')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (62, N'Itagüí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (63, N'Ituango')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (64, N'Jardín')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (65, N'Jericó')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (66, N'La Ceja')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (67, N'La Estrella')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (68, N'La Pintada')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (69, N'La Unión')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (70, N'Liborina')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (71, N'Maceo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (72, N'Marinilla')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (73, N'Montebello')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (74, N'Murindó')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (75, N'Mutatá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (76, N'Nariño')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (77, N'Necoclí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (78, N'Nechí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (79, N'Olaya')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (80, N'Peñol')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (81, N'Peque')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (82, N'Pueblorrico')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (83, N'Puerto Berrío')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (84, N'Puerto Nare')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (85, N'Puerto Triunfo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (86, N'Remedios')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (87, N'Retiro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (88, N'Rionegro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (89, N'Sabanalarga')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (90, N'Sabaneta')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (91, N'Salgar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (92, N'San Andrés')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (93, N'San Carlos')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (94, N'San francisco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (95, N'San Jerónimo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (96, N'San José de Montaña')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (97, N'San Juan de Urabá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (98, N'San Luis')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (99, N'San Pedro')
GO
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (100, N'San Pedro de Urabá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (101, N'San Rafael')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (102, N'San Roque')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (103, N'San Vicente')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (104, N'Santa Bárbara')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (105, N'Santa Rosa de Osos')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (106, N'Santo Domingo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (107, N'Santuario')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (108, N'Segovia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (109, N'Sonsón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (110, N'Sopetrán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (111, N'Támesis')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (112, N'Tarazá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (113, N'Tarso')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (114, N'Titiribí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (115, N'Toledo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (116, N'Turbo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (117, N'Uramita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (118, N'Urrao')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (119, N'Valdivia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (120, N'Valparaíso')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (121, N'Vegachí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (122, N'Venecia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (123, N'Vigía del Fuerte')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (124, N'Yalí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (125, N'Yarumal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (126, N'Yolombó')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (127, N'Yondó (Casabe)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (128, N'Zaragoza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (129, N'Arauca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (130, N'Arauquita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (131, N'Cravo Norte')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (132, N'Fortul')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (133, N'Puerto Rondón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (134, N'Saravena')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (135, N'Tame')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (136, N'Barranquilla')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (137, N'Baranoa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (138, N'Campo de la Cruz')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (139, N'Candelaria')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (140, N'Galapa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (141, N'Juan de Acosta')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (142, N'Luruaco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (143, N'Malambo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (144, N'Manatí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (145, N'Palmar de Varela')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (146, N'Piojó')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (147, N'Polonuevo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (148, N'Ponedera')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (149, N'Puerto Colombia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (150, N'Repelón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (151, N'Sabanagrande')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (152, N'Santa Lucía')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (153, N'Santo Tomás')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (154, N'Soledad')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (155, N'Suán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (156, N'Tubará')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (157, N'Usiacurí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (158, N'Cartagena')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (159, N'Achí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (160, N'Altos del Rosario')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (161, N'Arenal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (162, N'Arjona')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (163, N'Arroyohondo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (164, N'Barranco de Loba')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (165, N'Calamar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (166, N'Cantagallo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (167, N'Cicuto')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (168, N'Córdoba')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (169, N'Clemencia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (170, N'El Carmen de Bolívar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (171, N'El Guamo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (172, N'El Peñón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (173, N'Hatillo de Loba')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (174, N'Magangue')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (175, N'Mahates')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (176, N'Margarita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (177, N'María la Baja')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (178, N'Montecristo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (179, N'Mompós')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (180, N'Morales')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (181, N'Pinillos')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (182, N'Regidor')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (183, N'Río Viejo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (184, N'San Cristóbal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (185, N'San Estanislao')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (186, N'San Fernando')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (187, N'San Jacinto')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (188, N'San Jacinto del Cauca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (189, N'San Juan Nepomuceno')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (190, N'San Martín de Loba')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (191, N'San Pablo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (192, N'Santa Catalina')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (193, N'Santa Rosa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (194, N'Santa Rosa del Sur')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (195, N'Simití')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (196, N'Soplaviento')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (197, N'Talaigua Nuevo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (198, N'Tiquisio (Puerto Rico)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (199, N'Turbaco')
GO
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (200, N'Turbaná')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (201, N'Villanueva')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (202, N'Zambrano')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (203, N'Tunja')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (204, N'Almeida')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (205, N'Aquitania')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (206, N'Arcabuco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (207, N'Belén')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (208, N'Berbeo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (209, N'Beteitiva')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (210, N'Boavita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (211, N'Boyacá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (212, N'Buenavista')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (213, N'Busbanzá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (214, N'Campohermoso')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (215, N'Cerinza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (216, N'Chinavita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (217, N'Chiquinquirá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (218, N'Chiscas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (219, N'Chita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (220, N'Chitaranque')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (221, N'Chivatá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (222, N'Ciénaga')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (223, N'Cómbita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (224, N'Coper')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (225, N'Corrales')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (226, N'Covarachia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (227, N'Cubar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (228, N'Cucaita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (229, N'Cuitiva')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (230, N'Chíquiza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (231, N'Chivor')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (232, N'Duitama')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (233, N'El Cocuy')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (234, N'El Espino')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (235, N'Firavitoba')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (236, N'Floresta')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (237, N'Gachantivá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (238, N'Gámeza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (239, N'Garagoa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (240, N'Guacamayas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (241, N'Guateque')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (242, N'Guayatá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (243, N'Guicán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (244, N'Iza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (245, N'Jenesano')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (246, N'Labranzagrande')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (247, N'La Capilla')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (248, N'La Victoria')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (249, N'La Ubita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (250, N'Villa de Leyva')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (251, N'Macanal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (252, N'Maripí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (253, N'Miraflores')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (254, N'Mongua')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (255, N'Monguí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (256, N'Moniquirá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (257, N'Motavita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (258, N'Muzo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (259, N'Nobsa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (260, N'Nuevo Colón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (261, N'Oicatá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (262, N'Otanche')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (263, N'Pachavita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (264, N'Páez')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (265, N'Paipa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (266, N'Pajarito')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (267, N'Panqueba')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (268, N'Pauna')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (269, N'Paya')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (270, N'Paz de Río')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (271, N'Pesca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (272, N'Pisva')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (273, N'Puerto Boyacá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (274, N'Quípama')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (275, N'Ramiquirí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (276, N'Ráquira')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (277, N'Rondón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (278, N'Saboyá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (279, N'Sáchica')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (280, N'Samacá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (281, N'San Eduardo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (282, N'San José de Pare')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (283, N'San Luis de Gaceno')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (284, N'San Mateo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (285, N'San Miguel de Sema')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (286, N'San Pablo de Borbur')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (287, N'Santana')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (288, N'Santa María')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (289, N'Santa Rosa de Viterbo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (290, N'Santa Sofía')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (291, N'Sativanorte')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (292, N'Sativasur')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (293, N'Siachoque')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (294, N'Soatá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (295, N'Socotá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (296, N'Socha')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (297, N'Sogamoso')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (298, N'Somondoco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (299, N'Sora')
GO
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (300, N'Sotaquirá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (301, N'Soracá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (302, N'Susacón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (303, N'Sutamarchán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (304, N'Sutatenza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (305, N'Tasco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (306, N'Tenza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (307, N'Tibaná')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (308, N'Tibasosa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (309, N'Tinjacá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (310, N'Tipacoque')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (311, N'Toca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (312, N'Toguí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (313, N'Tópaga')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (314, N'Tota')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (315, N'Tunungua')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (316, N'Turmequé')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (317, N'Tuta')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (318, N'Tutazá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (319, N'Úmbita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (320, N'Ventaquemada')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (321, N'Viracachá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (322, N'Zetaquirá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (323, N'Manizales')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (324, N'Aguadas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (325, N'Anserma')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (326, N'Aranzazu')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (327, N'Belalcázar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (328, N'Chinchina')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (329, N'Filadelfia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (330, N'La Dorada')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (331, N'La Merced')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (332, N'Manzanares')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (333, N'Marmato')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (334, N'Marquetalia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (335, N'Marulanda')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (336, N'Neira')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (337, N'Pácora')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (338, N'Palestina')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (339, N'Pensilvania')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (340, N'Riosucio')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (341, N'Risaralda')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (342, N'Salamina')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (343, N'Samaná')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (344, N'San José')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (345, N'Supía')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (346, N'Victoria')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (347, N'Villamaría')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (348, N'Viterbo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (349, N'Florencia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (350, N'Albania')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (351, N'Belén de los Andaquíes')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (352, N'Cartagena del Chairá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (353, N'Curillo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (354, N'El Doncello')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (355, N'El Paujil')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (356, N'La Montañita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (357, N'Milán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (358, N'Morelia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (359, N'Puerto Rico')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (360, N'San José del Fragua')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (361, N'San Vicente del Caguán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (362, N'Solano')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (363, N'Solita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (364, N'Yopal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (365, N'Aguazul')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (366, N'Chameza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (367, N'Hato Corozal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (368, N'La Salina')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (369, N'Maní')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (370, N'Monterrey')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (371, N'Nunchía')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (372, N'Orocué')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (373, N'Paz de Ariporo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (374, N'Pore')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (375, N'Recetor')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (376, N'Sabalarga')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (377, N'Sácama')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (378, N'San Luis de Palenque')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (379, N'Támara')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (380, N'Tauramena')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (381, N'Trinidad')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (382, N'Popayán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (383, N'Almaguer')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (384, N'Balboa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (385, N'Buenos Aires')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (386, N'Cajibío')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (387, N'Caldono')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (388, N'Caloto')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (389, N'Corinto')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (390, N'El Tambo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (391, N'Guapi')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (392, N'Inzá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (393, N'Jambaló')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (394, N'La Sierra')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (395, N'La Vega')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (396, N'López (Micay)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (397, N'Mercaderes')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (398, N'Miranda')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (399, N'Padilla')
GO
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (400, N'Páez (Belalcazar)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (401, N'Patía (El Bordo)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (402, N'Piamonte')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (403, N'Piendamó')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (404, N'Puerto Tejada')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (405, N'Puracé (Coconuco)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (406, N'Rosas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (407, N'San Sebastián')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (408, N'Santander de Quilichao')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (409, N'Silvia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (410, N'Sotará (Paispamba)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (411, N'Suárez')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (412, N'Timbío')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (413, N'Timbiquí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (414, N'Toribío')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (415, N'Totoro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (416, N'Valledupar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (417, N'Aguachica')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (418, N'Agustín Codazzi')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (419, N'Astrea')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (420, N'Becerril')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (421, N'Bosconia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (422, N'Chimichagua')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (423, N'Chiriguaná')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (424, N'Curumaní')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (425, N'El Copey')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (426, N'El Paso')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (427, N'Gamarra')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (428, N'González')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (429, N'La Gloria')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (430, N'La Jagua de Ibirico')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (431, N'Manaure Balcón Cesar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (432, N'Pailitas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (433, N'Pelaya')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (434, N'Pueblo Bello')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (435, N'Río de Oro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (436, N'La Paz (Robles)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (437, N'San Alberto')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (438, N'San Diego')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (439, N'San Martín')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (440, N'Tamalameque')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (441, N'Montería')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (442, N'Ayapel')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (443, N'Canalete')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (444, N'Cereté')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (445, N'Chima')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (446, N'Chinú')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (447, N'Ciénaga de Oro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (448, N'Cotorra')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (449, N'La Apartada (Frontera)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (450, N'Lorica')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (451, N'Los Córdobas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (452, N'Momil')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (453, N'Montelíbano')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (454, N'Monitos')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (455, N'Planeta Rica')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (456, N'Pueblo Nuevo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (457, N'Puerto Escondido')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (458, N'Puerto Libertador')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (459, N'Purísima')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (460, N'Sahagún')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (461, N'San Andrés Sotavento')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (462, N'San Antero')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (463, N'San Bernardo del Viento')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (464, N'San Pelayo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (465, N'Tierralta')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (466, N'Valencia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (467, N'Agua de Dios')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (468, N'Albán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (469, N'Anapoima')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (470, N'Anolaima')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (471, N'Arbeláez')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (472, N'Beltrán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (473, N'Bituima')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (474, N'Bojacá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (475, N'Cabrera')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (476, N'Cachipay')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (477, N'Cajicá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (478, N'Caparrapí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (479, N'Cáqueza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (480, N'Carmen de Carupa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (481, N'Chaguaní')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (482, N'Chía')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (483, N'Chipaque')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (484, N'Choachí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (485, N'Chocontá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (486, N'Cogua')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (487, N'Cota')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (488, N'Cucunubá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (489, N'El Colegio')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (490, N'El Rosal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (491, N'Facatativá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (492, N'Fómeque')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (493, N'Fosca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (494, N'Funza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (495, N'Fúquene')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (496, N'Fusagasugá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (497, N'Gachalá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (498, N'Gachancipá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (499, N'Gachetá')
GO
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (500, N'Gama')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (501, N'Girardot')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (502, N'Guachetá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (503, N'Guaduas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (504, N'Guasca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (505, N'Guataquí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (506, N'Guatavita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (507, N'Guayabal de Síquima')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (508, N'Guayabetal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (509, N'Gutiérrez')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (510, N'Jerusalén')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (511, N'Junín')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (512, N'La Calera')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (513, N'La Mesa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (514, N'La Palma')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (515, N'La Peña')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (516, N'Lenguazaque')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (517, N'Machetá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (518, N'Madrid')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (519, N'Manta')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (520, N'Medina')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (521, N'Mosquera')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (522, N'Nemocón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (523, N'Nilo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (524, N'Nimaima')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (525, N'Nocaima')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (526, N'Venecia (Ospina Pérez)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (527, N'Pacho')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (528, N'Paime')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (529, N'Pandi')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (530, N'Paratebueno')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (531, N'Pasca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (532, N'Puerto Salgar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (533, N'Pulí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (534, N'Quebradanegra')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (535, N'Quetame')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (536, N'Quipile')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (537, N'Rafael')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (538, N'Ricaurte')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (539, N'San Antonio de Tequendama')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (540, N'San Bernardo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (541, N'San Cayetano')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (542, N'San Juan de Rioseco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (543, N'Sasaima')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (544, N'Sesquilé')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (545, N'Sibate')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (546, N'Silvania')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (547, N'Simijaca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (548, N'Soacha')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (549, N'Sopó')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (550, N'Subachoque')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (551, N'Suesca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (552, N'Supatá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (553, N'Susa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (554, N'Sutatausa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (555, N'Tabio')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (556, N'Tausa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (557, N'Tena')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (558, N'Tenjo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (559, N'Tibacuy')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (560, N'Tibiritá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (561, N'Tocaima')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (562, N'Tocancipá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (563, N'Topaipí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (564, N'Ubalá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (565, N'Ubaque')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (566, N'Ubaté')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (567, N'Une')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (568, N'Útica')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (569, N'Vergara')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (570, N'Vianí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (571, N'Villagómez')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (572, N'Villapinzón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (573, N'Villeta')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (574, N'Viotá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (575, N'Yacopí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (576, N'Zipacón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (577, N'Zipaquirá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (578, N'Quibdó')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (579, N'Acandí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (580, N'Alto Baudó (Pie de Pato)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (581, N'Atrato (Yuto)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (582, N'Bagadó')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (583, N'Bahía Solano (Mútis)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (584, N'Bajo Baudó (Pizarro)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (585, N'Bojayá (Bellavista)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (586, N'Cantón de San Pablo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (587, N'Condoto')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (588, N'El Carmen')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (589, N'El Litoral de San Juan')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (590, N'Itsmina')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (591, N'Juradó')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (592, N'Lloró')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (593, N'Nóvita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (594, N'Nuquí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (595, N'San José del Palmar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (596, N'Sipí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (597, N'Tadó')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (598, N'Unguía')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (599, N'Puerto Inírida')
GO
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (600, N'San José del Guaviare')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (601, N'El Retorno')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (602, N'Neiva')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (603, N'Acevedo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (604, N'Agrado')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (605, N'Aipe')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (606, N'Algeciras')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (607, N'Altamira')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (608, N'Baraya')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (609, N'Campoalegre')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (610, N'Colombia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (611, N'Elías')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (612, N'Garzón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (613, N'Gigante')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (614, N'Hobo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (615, N'Iquira')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (616, N'Isnos')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (617, N'La Argentina')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (618, N'La Plata')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (619, N'Nátaga')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (620, N'Oporapa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (621, N'Paicol')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (622, N'Palermo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (623, N'Pital')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (624, N'Pitalito')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (625, N'Rivera')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (626, N'Saladoblanco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (627, N'San Agustín')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (628, N'Suazá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (629, N'Tarqui')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (630, N'Tesalia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (631, N'Tello')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (632, N'Teruel')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (633, N'Timaná')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (634, N'Villavieja')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (635, N'Yaguará')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (636, N'Riohacha')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (637, N'Barrancas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (638, N'Dibulla')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (639, N'Distracción')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (640, N'El Molino')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (641, N'Fonseca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (642, N'Hatonuevo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (643, N'Maicao')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (644, N'Manaure')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (645, N'San Juan del Cesar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (646, N'Uribía')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (647, N'Urumita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (648, N'Santa Marta')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (649, N'Aracataca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (650, N'Ariguaní (El Difícil)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (651, N'Cerro San Antonio')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (652, N'Chivolo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (653, N'El Banco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (654, N'El Piñón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (655, N'El Retén')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (656, N'Fundación')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (657, N'Guamal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (658, N'Pedraza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (659, N'Pijiño del Carmen')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (660, N'Pivijay')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (661, N'Plato')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (662, N'Publoviejo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (663, N'Remolino')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (664, N'San Sebastián de Buuenavista')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (665, N'San Zenón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (666, N'Santa Ana')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (667, N'Sitionuevo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (668, N'Tenerife')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (669, N'Villavicencio')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (670, N'Acacias')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (671, N'Barranca de Upía')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (672, N'Cabuyaro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (673, N'Castilla la Nueva')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (674, N'Cubarral')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (675, N'Cumaral')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (676, N'El Calvario')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (677, N'El Castillo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (678, N'El Dorado')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (679, N'Fuente de Oro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (680, N'Mapiripán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (681, N'Mesetas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (682, N'La Macarena')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (683, N'La Uribe')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (684, N'Lejanías')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (685, N'Puerto Concordia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (686, N'Puerto Gaitán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (687, N'Puerto López')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (688, N'Puerto Lleras')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (689, N'Restrepo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (690, N'San Carlos de Guaroa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (691, N'San Juan de Arama')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (692, N'San Juanito')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (693, N'Vistahermosa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (694, N'Pasto')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (695, N'Albán (San José)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (696, N'Aldana')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (697, N'Ancuyá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (698, N'Arboleda (Berruecos)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (699, N'Barbacoas')
GO
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (700, N'Buesaco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (701, N'Colón (Génova)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (702, N'Consacá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (703, N'Contadero')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (704, N'Cuaspud (Carlosama)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (705, N'Cumbal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (706, N'Cumbitará')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (707, N'Chachagüi')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (708, N'El Charco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (709, N'El Rosario')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (710, N'El Tablón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (711, N'Funes')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (712, N'Guachucal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (713, N'Guaitarilla')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (714, N'Gualmatán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (715, N'Iles')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (716, N'Imúes')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (717, N'Ipiales')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (718, N'La Cruz')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (719, N'La Florida')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (720, N'La Llanada')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (721, N'La Tola')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (722, N'Leiva')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (723, N'Linares')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (724, N'Los Andes (Sotomayor)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (725, N'Magüí (Payán)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (726, N'Mallama (Piedrancha)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (727, N'Ospina')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (728, N'Francisco Pizarro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (729, N'Policarpa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (730, N'Potosí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (731, N'Providencia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (732, N'Puerres')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (733, N'Pupiales')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (734, N'Roberto Payán (San José)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (735, N'Samaniego')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (736, N'Sandoná')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (737, N'San Lorenzo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (738, N'San Pedro de Cartago')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (739, N'Santa Bárbara (Iscuandé)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (740, N'Santa Cruz (Guachávez)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (741, N'Sapuyés')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (742, N'Taminango')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (743, N'Tangua')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (744, N'Tumaco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (745, N'Túquerres')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (746, N'Yacuanquer')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (747, N'Cúcuta')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (748, N'Abrego')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (749, N'Arboledas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (750, N'Bochalema')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (751, N'Bucarasica')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (752, N'Cácota')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (753, N'Cáchira')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (754, N'Chinácota')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (755, N'Chitagá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (756, N'Convención')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (757, N'Cucutilla')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (758, N'Durania')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (759, N'El Tarra')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (760, N'El Zulia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (761, N'Gramalote')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (762, N'Hacarí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (763, N'Herrán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (764, N'Labateca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (765, N'La Esperanza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (766, N'La Playa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (767, N'Los Patios')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (768, N'Lourdes')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (769, N'Mutiscua')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (770, N'Ocaña')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (771, N'Pamplona')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (772, N'Pamplonita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (773, N'Puerto Santander')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (774, N'Ragonvalia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (775, N'Salazar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (776, N'San Calixto')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (777, N'Santiago')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (778, N'Sardinata')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (779, N'Silos')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (780, N'Teorama')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (781, N'Tibú')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (782, N'Villacaro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (783, N'Villa del Rosario')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (784, N'Mocoa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (785, N'Colón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (786, N'Orito')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (787, N'Puerto Asís')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (788, N'Puerto Caicedo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (789, N'Puerto Guzmán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (790, N'Puerto Leguízamo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (791, N'Sibundoy')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (792, N'San Miguel')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (793, N'Villa Gamuez (La Hormiga)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (794, N'Villa Garzón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (795, N'Calarcá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (796, N'Circasia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (797, N'Filandia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (798, N'Génova')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (799, N'La Tebaida')
GO
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (800, N'Montenegro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (801, N'Pijao')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (802, N'Quimbaya')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (803, N'Salento')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (804, N'Pereira')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (805, N'Apía')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (806, N'Belén de Umbría')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (807, N'Dos Quebradas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (808, N'Guática')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (809, N'La Celia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (810, N'La Virginia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (811, N'Marsella')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (812, N'Mistrató')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (813, N'Pueblo Rico')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (814, N'Quinchia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (815, N'Santa Rosa de Cabal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (816, N'Bucaramanga')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (817, N'Aguada')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (818, N'Aratoca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (819, N'Barichara')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (820, N'Barrancabermeja')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (821, N'California')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (822, N'Capitanejo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (823, N'Carcasí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (824, N'Cepitá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (825, N'Cerrito')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (826, N'Charalá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (827, N'Charta')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (828, N'Chipatá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (829, N'Cimitarra')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (830, N'Confines')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (831, N'Contratación')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (832, N'Coromoro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (833, N'Curití')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (834, N'El Guacamayo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (835, N'El Playón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (836, N'Encino')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (837, N'Enciso')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (838, N'Florián')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (839, N'Floridablanca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (840, N'Galán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (841, N'Gámbita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (842, N'Girón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (843, N'Guaca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (844, N'Guapotá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (845, N'Guavata')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (846, N'Guepsa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (847, N'Hato')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (848, N'Jesús María')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (849, N'Jordán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (850, N'La Belleza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (851, N'Landázuri')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (852, N'La Paz')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (853, N'Lebrija')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (854, N'Los Santos')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (855, N'Macaravita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (856, N'Málaga')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (857, N'Matanza')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (858, N'Mogotes')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (859, N'Molagavita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (860, N'Ocamonte')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (861, N'Oiba')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (862, N'Onzága')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (863, N'Palmar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (864, N'Palmas del Socorro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (865, N'Páramo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (866, N'Pie de Cuesta')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (867, N'Pinchote')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (868, N'Puente Nacional')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (869, N'Puerto Parra')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (870, N'Puerto Wilches')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (871, N'Sabana de Torres')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (872, N'San Benito')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (873, N'San Gil')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (874, N'San Joaquín')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (875, N'San José de Miranda')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (876, N'San Vicente de Chucurí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (877, N'Santa Helena del Opón')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (878, N'Simacota')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (879, N'Socorro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (880, N'Suaita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (881, N'Sucre')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (882, N'Suratá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (883, N'Tona')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (884, N'Valle de San José')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (885, N'Vélez')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (886, N'Vetas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (887, N'Zapatoca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (888, N'Sincelejo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (889, N'Caimito')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (890, N'Coloso (Ricaurte)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (891, N'Corozal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (892, N'Chalán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (893, N'Galeras (Nueva Granada)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (894, N'Guarandá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (895, N'Los Palmitos')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (896, N'Majagual')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (897, N'Morroa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (898, N'Ovejas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (899, N'Palmito')
GO
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (900, N'Sampués')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (901, N'San Benito Abad')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (902, N'San Juan de Betulia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (903, N'San Marcos')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (904, N'San Onofre')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (905, N'Sincé')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (906, N'Tolú')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (907, N'Toluviejo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (908, N'Ibagué')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (909, N'Alpujarra')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (910, N'Alvarado')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (911, N'Ambalema')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (912, N'Anzóategui')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (913, N'Armero (Guayabal)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (914, N'Ataco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (915, N'Cajamarca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (916, N'Carmen de Apicalá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (917, N'Casabianca')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (918, N'Chaparral')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (919, N'Coello')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (920, N'Coyaima')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (921, N'Cunday')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (922, N'Dolores')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (923, N'Espinal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (924, N'Falán')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (925, N'Flandes')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (926, N'Fresno')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (927, N'Guamo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (928, N'Herveo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (929, N'Honda')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (930, N'Icononzo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (931, N'Lérida')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (932, N'Líbano')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (933, N'Mariquita')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (934, N'Melgar')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (935, N'Murillo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (936, N'Natagaima')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (937, N'Ortega')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (938, N'Palocabildo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (939, N'Piedras')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (940, N'Planadas')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (941, N'Prado')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (942, N'Purificación')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (943, N'Rioblanco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (944, N'Roncesvalles')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (945, N'Rovira')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (946, N'Saldaña')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (947, N'San Antonio')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (948, N'Santa Isabel')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (949, N'Valle de San Juan')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (950, N'Venadillo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (951, N'Villahermosa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (952, N'Villarrica')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (953, N'Cali')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (954, N'Alcalá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (955, N'Andalucía')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (956, N'Ansermanuevo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (957, N'Buenaventura')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (958, N'Buga')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (959, N'Bugalagrande')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (960, N'Caicedonia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (961, N'Calima (Darién)')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (962, N'Cartago')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (963, N'Dagua')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (964, N'El Águila')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (965, N'El Cairo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (966, N'El Cerrito')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (967, N'El Dovio')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (968, N'Florida')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (969, N'Ginebra')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (970, N'Guacarí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (971, N'Jamundí')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (972, N'La Cumbre')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (973, N'Obando')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (974, N'Palmira')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (975, N'Pradera')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (976, N'Riofrío')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (977, N'Roldanillo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (978, N'Sevilla')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (979, N'Toro')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (980, N'Trujillo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (981, N'Tuluá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (982, N'Ulloa')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (983, N'Versalles')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (984, N'Vijes')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (985, N'Yotoco')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (986, N'Yumbo')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (987, N'Zarzal')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (988, N'Mitú')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (989, N'Carurú')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (990, N'Tatamá')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (991, N'Puerto Carreño')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (992, N'La Primavera')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (993, N'Santa Rosalia')
INSERT [dbo].[Ciudades] ([IdCiudad], [Descripcion]) VALUES (994, N'Cumaribo')
SET IDENTITY_INSERT [dbo].[Ciudades] OFF
SET IDENTITY_INSERT [dbo].[Clientes] ON 

INSERT [dbo].[Clientes] ([IdCliente], [Cedula], [Nombres], [Apellidos], [FechaNacimiento], [Genero], [IdPais], [IdDepartamento], [IdCiudad], [TipoDireccion], [Direccion], [Telefono], [Celular], [EstadoCivil], [Hijos], [Email], [Twitter], [Facebook], [HoraInicio], [HoraFin], [Emisoras], [RecibirCorreo], [RecibirSms], [Acepta], [IdDigitador], [Estado], [LlamadoPor], [FechaLLamada], [DuracionLlamada], [EstadoLlamada], [FechaCreacion], [Barrio], [Observacion], [IdEmpresa], [IdOcupacion]) VALUES (1, N'1022973561', N'Juan David', N'Maldonado Gómez', CAST(N'1992-01-11 00:00:00.000' AS DateTime), N'm', 45, 2, 62, N'carrera', N'Carrera 49 # 72 27 P 2', NULL, N'3046504259', N'Soltero', 0, N'maldo.s@hotmail.com', N'@jdmaldonado', N'/jdmaldonado', N'8:00', N'10:00', N'La X, Los 40 Principales', 1, 1, 1, 2, 0, NULL, NULL, 0, NULL, NULL, N'Santa María', NULL, 1, 1)
SET IDENTITY_INSERT [dbo].[Clientes] OFF
SET IDENTITY_INSERT [dbo].[Departamentos] ON 

INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (1, N'Amazonas')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (2, N'Antioquia')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (3, N'Arauca')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (4, N'Atlántico')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (5, N'Bolívar')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (6, N'Boyacá')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (7, N'Caldas')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (8, N'Caquetá')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (9, N'Casanare')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (10, N'Cauca')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (11, N'Cesar')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (12, N'Chocó')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (13, N'Córdoba')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (14, N'Cundinamarca')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (15, N'Güainia')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (16, N'Guaviare')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (17, N'Huila')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (18, N'La Guajira')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (19, N'Magdalena')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (20, N'Meta')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (21, N'Nariño')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (22, N'Norte de Santander')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (23, N'Putumayo')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (24, N'Quindío')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (25, N'Risaralda')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (26, N'San Andrés y Providencia')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (27, N'Santander')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (28, N'Sucre')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (29, N'Tolima')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (30, N'Valle del Cauca')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (31, N'Vaupés')
INSERT [dbo].[Departamentos] ([IdDepartamento], [Descripcion]) VALUES (32, N'Vichada')
SET IDENTITY_INSERT [dbo].[Departamentos] OFF
SET IDENTITY_INSERT [dbo].[Empresas] ON 

INSERT [dbo].[Empresas] ([IdEmpresa], [Nombre]) VALUES (1, N'Candela')
SET IDENTITY_INSERT [dbo].[Empresas] OFF
SET IDENTITY_INSERT [dbo].[Ocupaciones] ON 

INSERT [dbo].[Ocupaciones] ([IdOcupacion], [Descripcion]) VALUES (2, N'Contador')
INSERT [dbo].[Ocupaciones] ([IdOcupacion], [Descripcion]) VALUES (4, N'Empleado')
INSERT [dbo].[Ocupaciones] ([IdOcupacion], [Descripcion]) VALUES (5, N'Independiente')
INSERT [dbo].[Ocupaciones] ([IdOcupacion], [Descripcion]) VALUES (1, N'Ingeniero de Sistemas')
INSERT [dbo].[Ocupaciones] ([IdOcupacion], [Descripcion]) VALUES (3, N'Profesor')
SET IDENTITY_INSERT [dbo].[Ocupaciones] OFF
SET IDENTITY_INSERT [dbo].[Operadores] ON 

INSERT [dbo].[Operadores] ([IdOperador], [Codigo], [Nombres], [Apellidos], [IdPerfil]) VALUES (2, 1234567890, N'Super', N'Administrador', 1)
SET IDENTITY_INSERT [dbo].[Operadores] OFF
SET IDENTITY_INSERT [dbo].[Paises] ON 

INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (1, N'Afganistán')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (2, N'Albania')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (3, N'Alemania')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (4, N'Algeria')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (5, N'Andorra')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (6, N'Angola')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (7, N'Anguila')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (8, N'Antártida')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (9, N'Antigua y Barbuda')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (10, N'Antillas Neerlandesas')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (11, N'Arabia Saudita')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (12, N'Argentina')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (13, N'Armenia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (14, N'Aruba')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (15, N'Australia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (16, N'Austria')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (17, N'Azerbayán')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (18, N'Bélgica')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (19, N'Bahamas')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (20, N'Bahrein')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (21, N'Bangladesh')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (22, N'Barbados')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (23, N'Belice')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (24, N'Benín')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (25, N'Bhután')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (26, N'Bielorrusia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (27, N'Birmania')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (28, N'Bolivia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (29, N'Bosnia y Herzegovina')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (30, N'Botsuana')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (31, N'Brasil')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (32, N'Brunéi')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (33, N'Bulgaria')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (34, N'Burkina Faso')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (35, N'Burundi')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (36, N'Cabo Verde')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (37, N'Camboya')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (38, N'Camerún')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (39, N'Canadá')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (40, N'Chad')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (41, N'Chile')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (42, N'China')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (43, N'Chipre')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (44, N'Ciudad del Vaticano')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (45, N'Colombia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (46, N'Comoras')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (47, N'Congo')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (48, N'Congo')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (49, N'Corea del Norte')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (50, N'Corea del Sur')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (51, N'Costa de Marfil')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (52, N'Costa Rica')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (53, N'Croacia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (54, N'Cuba')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (55, N'Dinamarca')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (56, N'Dominica')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (57, N'Ecuador')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (58, N'Egipto')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (59, N'El Salvador')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (60, N'Emiratos Ãrabes Unidos')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (61, N'Eritrea')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (62, N'Eslovaquia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (63, N'Eslovenia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (64, N'España')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (65, N'Estados Unidos de América')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (66, N'Estonia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (67, N'Etiopía')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (68, N'Filipinas')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (69, N'Finlandia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (70, N'Fiyi')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (71, N'Francia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (72, N'Gabón')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (73, N'Gambia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (74, N'Georgia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (75, N'Ghana')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (76, N'Gibraltar')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (77, N'Granada')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (78, N'Grecia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (79, N'Groenlandia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (80, N'Guadalupe')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (81, N'Guam')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (82, N'Guatemala')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (83, N'Guayana Francesa')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (84, N'Guernsey')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (85, N'Guinea')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (86, N'Guinea Ecuatorial')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (87, N'Guinea-Bissau')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (88, N'Guyana')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (89, N'Haití')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (90, N'Honduras')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (91, N'Hong kong')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (92, N'Hungría')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (93, N'India')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (94, N'Indonesia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (95, N'Irán')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (96, N'Irak')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (97, N'Irlanda')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (98, N'Isla Bouvet')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (99, N'Isla de Man')
GO
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (100, N'Isla de Navidad')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (101, N'Isla Norfolk')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (102, N'Islandia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (103, N'Islas Bermudas')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (104, N'Islas Caimán')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (105, N'Islas Cocos (Keeling)')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (106, N'Islas Cook')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (107, N'Islas de Ã…land')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (108, N'Islas Feroe')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (109, N'Islas Georgias del Sur y Sandwich del Sur')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (110, N'Islas Heard y McDonald')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (111, N'Islas Maldivas')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (112, N'Islas Malvinas')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (113, N'Islas Marianas del Norte')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (114, N'Islas Marshall')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (115, N'Islas Pitcairn')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (116, N'Islas Salomón')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (117, N'Islas Turcas y Caicos')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (118, N'Islas Ultramarinas Menores de Estados Unidos')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (119, N'Islas Vírgenes Británicas')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (120, N'Islas Vírgenes de los Estados Unidos')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (121, N'Israel')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (122, N'Italia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (123, N'Jamaica')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (124, N'Japón')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (125, N'Jersey')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (126, N'Jordania')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (127, N'Kazajistán')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (128, N'Kenia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (129, N'Kirgizstán')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (130, N'Kiribati')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (131, N'Kuwait')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (132, N'Líbano')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (133, N'Laos')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (134, N'Lesoto')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (135, N'Letonia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (136, N'Liberia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (137, N'Libia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (138, N'Liechtenstein')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (139, N'Lituania')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (140, N'Luxemburgo')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (141, N'México')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (142, N'Mónaco')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (143, N'Macao')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (144, N'Macedonia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (145, N'Madagascar')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (146, N'Malasia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (147, N'Malawi')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (148, N'Mali')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (149, N'Malta')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (150, N'Marruecos')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (151, N'Martinica')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (152, N'Mauricio')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (153, N'Mauritania')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (154, N'Mayotte')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (155, N'Micronesia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (156, N'Moldavia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (157, N'Mongolia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (158, N'Montenegro')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (159, N'Montserrat')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (160, N'Mozambique')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (161, N'Namibia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (162, N'Nauru')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (163, N'Nepal')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (164, N'Nicaragua')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (165, N'Niger')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (166, N'Nigeria')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (167, N'Niue')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (168, N'Noruega')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (169, N'Nueva Caledonia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (170, N'Nueva Zelanda')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (171, N'Omán')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (172, N'Países Bajos')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (173, N'Pakistán')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (174, N'Palau')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (175, N'Palestina')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (176, N'Panamá')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (177, N'Papúa Nueva Guinea')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (178, N'Paraguay')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (179, N'Perú')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (180, N'Polinesia Francesa')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (181, N'Polonia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (182, N'Portugal')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (183, N'Puerto Rico')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (184, N'Qatar')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (185, N'Reino Unido')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (186, N'República Centroafricana')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (187, N'República Checa')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (188, N'República Dominicana')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (189, N'Reunión')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (190, N'Ruanda')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (191, N'Rumanía')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (192, N'Rusia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (193, N'Sahara Occidental')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (194, N'Samoa')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (195, N'Samoa Americana')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (196, N'San Bartolomé')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (197, N'San Cristóbal y Nieves')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (198, N'San Marino')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (199, N'San Martín (Francia)')
GO
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (200, N'San Pedro y Miquelón')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (201, N'San Vicente y las Granadinas')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (202, N'Santa Elena')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (203, N'Santa Lucía')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (204, N'Santo Tomé y Príncipe')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (205, N'Senegal')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (206, N'Serbia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (207, N'Seychelles')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (208, N'Sierra Leona')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (209, N'Singapur')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (210, N'Siria')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (211, N'Somalia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (212, N'Sri lanka')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (213, N'Sudáfrica')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (214, N'Sudán')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (215, N'Suecia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (216, N'Suiza')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (217, N'Surinám')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (218, N'Svalbard y Jan Mayen')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (219, N'Swazilandia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (220, N'Tadjikistán')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (221, N'Tailandia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (222, N'Taiwán')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (223, N'Tanzania')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (224, N'Territorio Británico del Océano Ãndico')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (225, N'Territorios Australes y Antárticas Franceses')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (226, N'Timor Oriental')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (227, N'Togo')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (228, N'Tokelau')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (229, N'Tonga')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (230, N'Trinidad y Tobago')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (231, N'Tunez')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (232, N'Turkmenistán')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (233, N'Turquía')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (234, N'Tuvalu')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (235, N'Ucrania')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (236, N'Uganda')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (237, N'Uruguay')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (238, N'Uzbekistán')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (239, N'Vanuatu')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (240, N'Venezuela')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (241, N'Vietnam')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (242, N'Wallis y Futuna')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (243, N'Yemen')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (244, N'Yibuti')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (245, N'Zambia')
INSERT [dbo].[Paises] ([IdPais], [Descripcion]) VALUES (246, N'Zimbabue')
SET IDENTITY_INSERT [dbo].[Paises] OFF
SET IDENTITY_INSERT [dbo].[Perfiles] ON 

INSERT [dbo].[Perfiles] ([IdPerfil], [Perfil]) VALUES (1, N'Administrador')
INSERT [dbo].[Perfiles] ([IdPerfil], [Perfil]) VALUES (2, N'Digitador')
INSERT [dbo].[Perfiles] ([IdPerfil], [Perfil]) VALUES (3, N'Asesor')
INSERT [dbo].[Perfiles] ([IdPerfil], [Perfil]) VALUES (4, N'Invitado')
SET IDENTITY_INSERT [dbo].[Perfiles] OFF
INSERT [dbo].[PerfilesRoles] ([IdPerfil], [IdRol]) VALUES (1, 1)
INSERT [dbo].[PerfilesRoles] ([IdPerfil], [IdRol]) VALUES (1, 2)
INSERT [dbo].[PerfilesRoles] ([IdPerfil], [IdRol]) VALUES (1, 3)
INSERT [dbo].[PerfilesRoles] ([IdPerfil], [IdRol]) VALUES (1, 4)
INSERT [dbo].[PerfilesRoles] ([IdPerfil], [IdRol]) VALUES (1, 5)
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([IdRol], [Rol], [Icono], [Href]) VALUES (1, N'Perfiles', N'glyphicon glyphicon-cog', N'profile')
INSERT [dbo].[Roles] ([IdRol], [Rol], [Icono], [Href]) VALUES (2, N'Digitador', N'glyphicon glyphicon-cloud-upload', N'type')
INSERT [dbo].[Roles] ([IdRol], [Rol], [Icono], [Href]) VALUES (3, N'Asesor', N'glyphicon glyphicon-earphone', N'call')
INSERT [dbo].[Roles] ([IdRol], [Rol], [Icono], [Href]) VALUES (4, N'Operadores', N'glyphicon glyphicon-user', N'operator')
INSERT [dbo].[Roles] ([IdRol], [Rol], [Icono], [Href]) VALUES (5, N'Clientes', N'glyphicon glyphicon-list-alt', N'client')
INSERT [dbo].[Roles] ([IdRol], [Rol], [Icono], [Href]) VALUES (6, N'Actualizar', N'glyphicon glyphicon-wrench', N'update')
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Clientes__B4ADFE3897F9963E]    Script Date: 21/04/2016 1:41:22 p. m. ******/
ALTER TABLE [dbo].[Clientes] ADD UNIQUE NONCLUSTERED 
(
	[Cedula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Cedula]    Script Date: 21/04/2016 1:41:22 p. m. ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Cedula] ON [dbo].[Clientes]
(
	[Cedula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Ocupacio__92C53B6C52DA46B8]    Script Date: 21/04/2016 1:41:22 p. m. ******/
ALTER TABLE [dbo].[Ocupaciones] ADD UNIQUE NONCLUSTERED 
(
	[Descripcion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ__Operador__06370DAC1A9AE4D7]    Script Date: 21/04/2016 1:41:22 p. m. ******/
ALTER TABLE [dbo].[Operadores] ADD UNIQUE NONCLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD FOREIGN KEY([IdCiudad])
REFERENCES [dbo].[Ciudades] ([IdCiudad])
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD FOREIGN KEY([IdDepartamento])
REFERENCES [dbo].[Departamentos] ([IdDepartamento])
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD FOREIGN KEY([IdDigitador])
REFERENCES [dbo].[Operadores] ([IdOperador])
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD FOREIGN KEY([IdPais])
REFERENCES [dbo].[Paises] ([IdPais])
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK_Clientes_Empresas] FOREIGN KEY([IdEmpresa])
REFERENCES [dbo].[Empresas] ([IdEmpresa])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK_Clientes_Empresas]
GO
ALTER TABLE [dbo].[Operadores]  WITH CHECK ADD FOREIGN KEY([IdPerfil])
REFERENCES [dbo].[Perfiles] ([IdPerfil])
GO
ALTER TABLE [dbo].[PerfilesRoles]  WITH CHECK ADD FOREIGN KEY([IdPerfil])
REFERENCES [dbo].[Perfiles] ([IdPerfil])
GO
ALTER TABLE [dbo].[PerfilesRoles]  WITH CHECK ADD FOREIGN KEY([IdRol])
REFERENCES [dbo].[Roles] ([IdRol])
GO
/****** Object:  StoredProcedure [dbo].[sp_GetRolesByIdProfile]    Script Date: 21/04/2016 1:41:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Juan David Maldonado>
-- Create date: <Create Date,31/08/2014,>
-- Description:	<Description, Devuelve los Role que tiene Activos (1) y los que no (0) de un Perfil,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRolesByIdProfile]
	@IdPerfil	SMALLINT
AS
BEGIN
	
	DECLARE @Perfil NVARCHAR(50)
	
	SELECT @Perfil = Perfil FROM Perfiles  WHERE IdPerfil = @IdPerfil


	SELECT
		@IdPerfil	AS IdPerfil
		,@Perfil	AS Perfil
		,r.[IdRol]
		,r.[Rol]
		,CASE WHEN (pr.[IdPerfil] IS NULL)
				THEN 0
				ELSE 1 END AS Activo		
	FROM [ClientsManagement].[dbo].[Roles] r WITH (NOLOCK)
			LEFT JOIN  [ClientsManagement].[dbo].[PerfilesRoles] pr	WITH (NOLOCK) ON r.IdRol		= pr.IdRol and pr.IdPerfil = @IdPerfil
	ORDER BY 1
END


GO
USE [master]
GO
ALTER DATABASE [ClientsManagement] SET  READ_WRITE 
GO
