﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace ClientManagement
{
    public class Logger
    {
        public static bool ErrorFlag = false;

        private static void CheckLogFolder()
        {
            if (!Directory.Exists(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(Logger)).CodeBase).Replace("file:\\", ""), "Log")))
            {
                Directory.CreateDirectory(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(Logger)).CodeBase).Replace("file:\\", ""), "Log"));
            }
        }

        private static string GetErrorLogFilePath()
        {
            string sRetVal;

            CheckLogFolder();

            sRetVal = Path.Combine(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(Logger)).CodeBase).Replace("file:\\", ""), "Log"), "Errors.txt");

            return sRetVal;
        }

        public static void LogException(Exception _ex)
        {
            string message = DateTime.Now.ToString() + "\r\n" + _ex.Message + "\r\n" + (_ex.InnerException == null ? "" : _ex.InnerException.ToString());

            Write(message, "ERROR");
        }

        public static void Write(string _message, string _where)
        {
            var sw = new StreamWriter(GetErrorLogFilePath(), true, Encoding.ASCII);
                        
            sw.WriteLine(_message);
            sw.Flush();           
        }
    }
}