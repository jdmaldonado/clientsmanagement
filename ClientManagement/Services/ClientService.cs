﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using ClientManagement.Models;
using ClientManagement.DataBase;
using OfficeOpenXml;
using OfficeOpenXml.Style;


namespace ClientManagement.Services
{
    public class ClientService
    {
        public Client SetOperatorIdForCreation(Client client, long operatorCode)
        {
            Operator _operator = DbManager.GetOperatorFromCode(operatorCode);

            client.IdDigitador = _operator.IdOperador;

            Client createdClient = DbManager.CreateClient(client);

            return createdClient;
        }

        public dynamic SetOperatorIdForUpdate(Client client, long operatorCode)
        {
            Operator _operator = DbManager.GetOperatorFromCode(operatorCode);

            var updateClient = DbManager.UpdateClient(client);

            return updateClient;
        }

        public dynamic SetOperatorIdForCall(Client client, long operatorCode)
        {
            Operator _operator = DbManager.GetOperatorFromCode(operatorCode);

            client.LlamadoPor = _operator.IdOperador;
            client.FechaLlamada = DateTime.Now;
            client.Estado = true; //true: Llamado, false: sin Llamar

            var calledClient = DbManager.SetClientToCalled(client);

            return calledClient;
        }

        public dynamic SetOperatorIdForCallStatus(Client client, long operatorCode)
        {
            Operator _operator = DbManager.GetOperatorFromCode(operatorCode);

            //client.ModificadoPor = _operator.IdOperador;

            var calledClient = DbManager.ChangeCallStatus(client);

            return calledClient;
        }

        public dynamic SetOperatorNameForGet()
        {

            List<Client> _clients = DbManager.GetClients();

            foreach (var _client in _clients)
            {

                Operator _operator = _client.LlamadoPor == null ? null : DbManager.GetOperatorFromId(_client.LlamadoPor.Value);

                if (_operator == null)
                {
                    continue;
                }
                else
                {
                    _client.Asesor = _operator.Nombres + " " + _operator.Apellidos;
                }

            }

            return _clients;
        }

        public List<Client> SetNamesForGrid(List<Client> clients)
        {

            foreach (var client in clients)
            {
                var operatorAsesor      = client.LlamadoPor == null ? null : DbManager.GetOperatorFromId(client.LlamadoPor.Value);
                var operatorDigitador   = DbManager.GetOperatorFromId(client.IdDigitador);
                var country             = DbManager.GetCountryById(client.IdPais);
                var city                = DbManager.GetCityById(client.IdCiudad);
                var department          = DbManager.GetDepartmentById(client.IdDepartamento);
                var occupation          = DbManager.GetOccupationById(client.IdOcupacion);

                client.Asesor       = operatorAsesor == null    ? null : operatorAsesor.Nombres     + " " + operatorAsesor.Apellidos;
                client.Digitador    = operatorDigitador == null ? null : operatorDigitador.Nombres  + " " + operatorDigitador.Apellidos;

                client.Pais         = country.Descripcion;
                client.Ciudad       = city.Descripcion;
                client.Departamento = department.Descripcion;
                client.Ocupacion    = (occupation == null)? "" : occupation.Descripcion;

            }

            return clients;
        }

        public Stream GenerateExcelFileForClients(List<Client> records, CultureInfo culture)
        {

            var headers = new List<object[]> {GetSpreadSheetHeader()};

            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Historical Clients");

            var recordsDecoded = new List<object[]>();

            foreach (var record in records)
                recordsDecoded.Add(ConvertClientToObject(record, culture));

            ws.Cells["A1"].LoadFromArrays(headers);

            using (ExcelRange rng = ws.Cells["A1:AH1"])
            {
                rng.Style.Font.Bold = true;
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                rng.Style.Font.Color.SetColor(Color.White);
            }

            if (recordsDecoded.Count > 0)
            {
                ws.Cells["A2"].LoadFromArrays(recordsDecoded);
            }

            return new MemoryStream(pck.GetAsByteArray());
        }

        private object[] GetSpreadSheetHeader()
        {
            return new object[] {
                                    "IdCliente", 
                                    "Cedula",
                                    "Nombres", 
                                    "Apellidos",
                                    "Fecha Nacimiento",
                                    "Género",
                                    "Pais",
                                    "Departamento",
                                    "Ciudad",
                                    "Tipo Dirección",
                                    "Dirección",
                                    "Barrio",
                                    "Teléfono",
                                    "Celular",
                                    "Estado Civil",
                                    "Hijos",
                                    "Correo",
                                    "Twitter",
                                    "Facebook",
                                    "Hora Inicial",
                                    "Hora Final",
                                    "Otras Emisoras",
                                    "Recibir Correo",
                                    "Recibir SMS",
                                    "Acepta Términos",
                                    "Digitador",
                                    "Estado Cliente",
                                    "Estado Llamada",
                                    "Asesor Llamada",
                                    "Duracion Llamada (segundos)",
                                    "Fecha Creación",
                                    "Observaciones",
                                    "Empresa",
                                    "Ocupación",
                                    "Fecha Modificación"
            };
        }

        private object[] ConvertClientToObject(Client client, CultureInfo culture)
        {
            var status = client.Estado? "Llamado" : "Sin Llamar";
            var statusCall = GetCallStatusString(client.EstadoLlamada);
            var hijos = client.Hijos ? "Con Hijos" : "Sin Hijos";
            var correos = client.RecibirCorreo? "SI" : "NO";
            var sms = client.RecibirSms? "SI" : "NO";
            var acepta = client.Acepta? "SI" : "NO";
            var fechaCreacion = client.FechaCreacion == null ? "CARGUE INICIAL" : client.FechaCreacion.ToString();
            var fechaNacimiento = client.FechaNacimiento == null ? "" : client.FechaNacimiento.ToString();
            var fechaModificacion = client.FechaModificacion == null ? "" : client.FechaModificacion.ToString();
            var empresa = client.IdEmpresa == 1 ? "Candela" : "Vibra";


            return new object[] { client.IdCliente.ToString(),
                                    client.Cedula,
                                    client.Nombres,
                                    client.Apellidos,
                                    fechaNacimiento,
                                    client.Genero,
                                    client.Pais,
                                    client.Departamento,
                                    client.Ciudad,
                                    client.TipoDireccion,
                                    client.Direccion,
                                    client.Barrio,
                                    client.Telefono,
                                    client.Celular,                                    
                                    client.EstadoCivil,
                                    hijos,
                                    client.Email,
                                    client.Twitter,
                                    client.Facebook,
                                    client.HoraInicio,
                                    client.HoraFin,
                                    client.Emisoras,
                                    correos,
                                    sms,
                                    acepta,
                                    client.Digitador,
                                    status,
                                    statusCall,
                                    client.Asesor,
                                    (client.DuracionLlamada/1000).ToString(),
                                    fechaCreacion,
                                    client.Observacion,
                                    empresa,
                                    client.Ocupacion,
                                    client.FechaModificacion
            };
        }

        private string GetCallStatusString(short? idCallStatus)
        {
            if (idCallStatus != null)
            {
                switch (idCallStatus)
                {
                    case 0: return "Existosa";
                    case 1: return "No Existosa";
                    case 2: return "No Contactar";
                    default: return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }            
        }
    }

}