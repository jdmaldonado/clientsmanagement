﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientManagement.Models
{
    public class Department
    {
        public int idDepartamento { get; set; }
        public string Descripcion { get; set; }
    }
}