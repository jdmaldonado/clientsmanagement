﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientManagement.Models
{
    public class Occupation
    {
        public int IdOcupacion      { get; set; }
        public string Descripcion   { get; set; }
    }
}