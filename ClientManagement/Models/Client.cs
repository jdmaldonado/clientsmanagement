﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientManagement.Models
{
    public class Client
    {
        public int IdCliente { get; set; }
        public long Cedula { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Genero { get; set; }
        public int IdPais { get; set; }
        public string Pais { get; set; }
        public int IdDepartamento { get; set; }
        public string Departamento { get; set; }
        public int IdCiudad { get; set; }
        public string Ciudad { get; set; }
        public string TipoDireccion { get; set; }
        public string Barrio { get; set; }
        public string Direccion { get; set; }        
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string EstadoCivil { get; set; }
        public Boolean Hijos { get; set; }
        public string Email { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }
        public string HoraInicio { get; set; }
        public string HoraFin { get; set; }
        public string Emisoras { get; set; }
        public Boolean RecibirCorreo { get; set; }
        public Boolean RecibirSms { get; set; }
        public Boolean Acepta { get; set; }
        public int IdDigitador { get; set; }
        public string Digitador { get; set; }
        public Boolean Estado { get; set; }
        public int? LlamadoPor { get; set; }
        public string Asesor { get; set; }        
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaLlamada { get; set; }
        public long DuracionLlamada { get; set; }
        public short? EstadoActualLlamada { get; set; }
        public short? EstadoLlamada { get; set; }
        public string Observacion { get; set; }
        public int IdEmpresa { get; set; }
        public int IdOcupacion { get; set; }
        public string Ocupacion { get; set; }
        public int Cant { get; set; }
        public DateTime? FechaModificacion { get; set; }

    }
}