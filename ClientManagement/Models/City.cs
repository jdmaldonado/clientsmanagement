﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientManagement.Models
{
    public class City
    {
        public int idCiudad { get; set; }
        public string Descripcion { get; set; }
    }
}