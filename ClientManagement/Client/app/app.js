﻿var CarrazoApp = angular.module("CarrazoApp", ['ngResource', 'ui.bootstrap', 'ngRoute', 'ngGrid', 'angular-loading-bar'])
    
    .value("toastr", toastr)

    .config(function ( $routeProvider, $httpProvider) {

      $routeProvider

        .when('/', {
            controller: 'authenticationCtrl',
            templateUrl: 'Client/app/templates/authenticationTemplate.html'
        })

        .when('/menu/:operatorCode', {
             controller: 'menuCtrl',
             templateUrl: 'Client/app/templates/menuTemplate.html'
        })

        .when('/type/:operatorCode', {
              controller: 'typeCtrl',
              templateUrl: 'Client/app/templates/typeTemplate.html'
        })

        .when('/profile/:operatorCode', {
            controller: 'profileCtrl',
            templateUrl: 'Client/app/templates/profileTemplate.html'
        })
          
        .when('/call/:operatorCode', {
            controller: 'callCtrl',
            templateUrl: 'Client/app/templates/callTemplate.html'
        })

        .when('/client/:operatorCode', {
            controller: 'clientCtrl',
            templateUrl: 'Client/app/templates/clientTemplate.html'
        })

        .when('/operator/:operatorCode', {
            controller: 'operatorCtrl',
            templateUrl: 'Client/app/templates/operatorTemplate.html'
        })

        .when('/update/:operatorCode', {
            controller: 'updateCtrl',
            templateUrl: 'Client/app/templates/updateTemplate.html'
        })

        .when('/occupation/:operatorCode', {
            controller: 'occupationCtrl',
            templateUrl: 'Client/app/templates/occupationTemplate.html'
        })


        .otherwise({ redirectTo: '/' });

        $httpProvider.interceptors.push('authInterceptor_factory');

  });
