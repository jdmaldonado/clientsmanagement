﻿CarrazoApp.controller('typeCtrl', function typeCtrl($scope, $filter, $rootScope, $routeParams, clients_Factory, cities_Factory, departments_Factory, countries_Factory, occupations_Factory) {

    /* Este controlador debe recibir el código del Operador para que pueda funcionar correctamente */
    $scope.operatorCode = $routeParams.operatorCode;

    $scope.view = {
        editInfo: true,
        form: false
    };
        
    $scope.formClient = {}; /* Formulario Básico */

    $scope.city = {};
    $scope.occupation = {};
    $scope.department = {};
    $scope.country = {};
    $scope.initHour = null;
    $scope.endHour = null;

    getCities = function () {
        $scope.ciudades = cities_Factory.city.GetAllCities({});
    }

    getFactories = function () {
        $scope.empresas = clients_Factory.getFactories.GetFactories({});
    }

    getDepartments = function () {
        $scope.departamentos = departments_Factory.department.GetAllDepartments({});
    }

    getCountries = function () {
        $scope.paises = countries_Factory.country.GetAllCountries({});
    }

    getOccupations = function () {
        $scope.ocupaciones = occupations_Factory.occupations.GetOccupations({});
    }

    function fixClientGenre(client) {
        client.genero = $filter('lowercase')(client.genero);
        return client;
    }

    getCities();
    getOccupations();
    getFactories();
    getDepartments();
    getCountries();
    

    /* Limpia Los formularios */
    $scope.Clean = function () {
        $scope.view.editInfo = true;
        $scope.view.form = false;
        $scope.identification = undefined;
        $scope.formClient = {};
        $scope.city = {};
        $scope.department = {};
        $scope.country = {};
    }

    /* Actualiza el formulario principal con la información del cliente seleccionado */
    $scope.Update = function (identification) {

        /* Primero Esconda el formulario de edición y muestre el básico */
        $scope.view.editInfo = false;
        $scope.view.form = true;

        /*Traemos*/
        clients_Factory.client.GetByNumberAndType({ number: identification, type: 'Cedula' }).$promise.then(function (data) {
            if (data.length > 0) {
                $scope.formClient = fixClientGenre(data[0]);

                $scope.ciudades.map(function (objeto) {
                    if (objeto.idCiudad == $scope.formClient.idCiudad) {
                        $scope.city = objeto;
                    }
                })

                $scope.ocupaciones.map(function (objeto) {
                    if (objeto.idOcupacion == $scope.formClient.idOcupacion) {
                        $scope.occupation = objeto;
                    }
                })

                $scope.empresas.map(function (objeto) {
                    if (objeto.idEmpresa == $scope.formClient.idEmpresa) {
                        $scope.factory = objeto;
                    }
                })

                $scope.departamentos.map(function (objeto) {
                    if (objeto.idDepartamento == $scope.formClient.idDepartamento) {
                        $scope.department = objeto;
                    }
                })

                $scope.paises.map(function (objeto) {
                    if (objeto.idPais == $scope.formClient.idPais) {
                        $scope.country = objeto;
                    }
                })
            }

            /*Esté o no Registrado el cliente, cargue en el formulario la cedula que acaba de escribir*/
            $scope.formClient.cedula = identification;
            
        });
    }

    $scope.Save = function () {

        /* Si Existe un idCliente Quiere decir que se va a EDITAR, 
        de lo contrario voy a CREAR uno nuevo*/

        $scope.formClient.idCiudad = $scope.city.idCiudad;
        $scope.formClient.ciudad = $scope.city.descripcion;

        $scope.formClient.idDepartamento = $scope.department.idDepartamento;
        $scope.formClient.departamento = $scope.department.descripcion;

        $scope.formClient.idPais = $scope.country.idPais;
        $scope.formClient.pais = $scope.country.descripcion;

        $scope.formClient.idEmpresa = $scope.factory.idEmpresa;
        $scope.formClient.idOcupacion = ($scope.occupation !== undefined) ? $scope.occupation.idOcupacion : null;


        if ($scope.formClient.idCliente === undefined) {
            clients_Factory.createClient.PostClient({ operatorCode: $scope.operatorCode }, $scope.formClient);
        }
        else {
            clients_Factory.updateClient.putClient({ idClient: $scope.formClient.idCliente, operatorCode: $scope.operatorCode }, $scope.formClient);
        }

        $scope.Clean();

    }

    /* DATEPICKER  */
    $scope.today = function () {
        $scope.formClient.fechaNacimiento = new Date();
    };

    $scope.cleanDate = function () {
        $scope.formClient.fechaNacimiento = null;
    };

    $scope.cleanDate();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.bornDateOpened = true;
    };

    $scope.format = 'dd/MM/yyyy';

    
});

