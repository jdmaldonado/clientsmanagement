﻿CarrazoApp.controller('occupationCtrl', function occupationCtrl($scope, $rootScope, $routeParams, occupations_Factory) {

    /* Este controlador debe recibir el código del Operador para que pueda funcionar correctamente */
    $scope.operatorCode = $routeParams.operatorCode;


    $scope.view = {
        editInfo: false,
        form: true
    };

    $scope.processing = true;
    $scope.formOccupation = {};

    getOccupations = function () {
        $scope.ocupaciones = occupations_Factory.occupations.GetOccupations({});
        console.log($scope.ocupaciones);
        $scope.processing = false;
    }


    getOccupations();

    $scope.loadEditForm = function () {
        $scope.view.editInfo = true;
        $scope.view.form = false;
        $scope.occupation = undefined;
    }

    $scope.Update = function (occupationDescription) {

        $scope.view.form = true;
        $scope.view.editInfo = false;


        $scope.ocupaciones.map(function (objeto) {
            if (angular.equals(objeto.descripcion, occupationDescription)) {
                $scope.formOccupation = objeto;
            }
        })
    }

    $scope.Limpiar = function () {
        $scope.formOccupation = {};
        $scope.view.editInfo = false;
        $scope.view.form = true;
        getOccupations();
    }

    $scope.Guardar = function () {

        if ($scope.formOccupation.idOcupacion === undefined) {

            occupations_Factory.createOccupation.postOccupation({}, $scope.formOccupation).$promise.then(function () {
                $scope.Limpiar();
            });
        }
        else {
            occupations_Factory.updateOccupation.putOccupation({ idOccupation: $scope.formOccupation.idOccupation }, $scope.formOccupation).$promise.then(function () {
                $scope.Limpiar();
            });
        }

    }

});

