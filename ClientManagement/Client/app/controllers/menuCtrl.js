﻿CarrazoApp.controller('menuCtrl', function menuCtrl($scope, $rootScope, $routeParams, menus_Factory) {

    $scope.menus = {};
    $scope.operatorCode = $routeParams.operatorCode;

    menus_Factory.menus.GetMenus({ operatorCode: $scope.operatorCode }).$promise.then(function (data) {
        $scope.menus = data;
    });
});

