﻿CarrazoApp.controller('clientCtrl', function clientCtrl($scope, $rootScope, $filter, $routeParams, clients_Factory, downloadSpreadsheet_factory) {

    $scope.filter = {};
    $scope.dates = {};

    $scope.clientStatus = [
        { value: true, text: "Llamados" },
        { value: false, text: "Sin llamar" }
    ];

    $scope.callStatus = [
       { value: 0, text: "Exitosa" },
       { value: 1, text: "No exitosa" },
       { value: 2, text: "No contactar" }
    ];

    $scope.evaluateCallStatus = function (status) {
        if (status === false) {
            $scope.filter.callStatus = null;
            $scope.dates.startCall = null;
            $scope.dates.endCall = null;
        }
    }

    //
    $scope.factory = {};

    /* Este controlador debe recibir el código del Operador para que pueda funcionar correctamente */
    $scope.operatorCode = $routeParams.operatorCode;

    $scope.empresas = clients_Factory.getFactories.GetFactories({});

    /* Pagination Variables */
        $scope.maxSize = 5; /* Cantidad de Botones que se muestran de la paginacion */
        $scope.page = { current : 1 }/* Página Actual */
        $scope.pageSize = 10; /* Cantidad de Items por cada Página + 1*/
    /**/

    $scope.enableDownload = false;
    $scope.showEmptyMessage = false;
    $scope.totalItems = 0;


    $scope.requestData = function () {

        $scope.showEmptyMessage = false;
        
        $scope.filter.startType = $filter('unixTime_Filter')($scope.dates.startType);
        $scope.filter.endType   = $filter('unixTime_Filter')($scope.dates.endType);
        $scope.filter.startCall = $filter('unixTime_Filter')($scope.dates.startCall);
        $scope.filter.endCall   = $filter('unixTime_Filter')($scope.dates.endCall);
        $scope.filter.page      = $scope.page.current;

        $scope.clientes = {};

        clients_Factory.clientsGrid.query($scope.filter).$promise.then(function (data) {
           
            if (data.length > 0) {
                $scope.totalItems = data[0].cant;
                $scope.clientes = data;
                $scope.enableDownload = true;
                $scope.showEmptyMessage = false;
            }
            else {
                $scope.showEmptyMessage = true;
            }
            
        });
    }

    $scope.exportData = function () {

        $scope.filter.format = 'spreadsheet';
        downloadSpreadsheet_factory.downloadIt($scope.filter, "Clients/Historical");

    }


    /* DATEPICKER  */
    $scope.todayStartTypeDate = function () {
        $scope.dates.startType = new Date();
    };

    $scope.todayFinalTypeDate = function () {
        $scope.dates.endType = new Date();
    };

    $scope.cleanStartTypeDate = function () {
        $scope.dates.startType = null;
    };

    $scope.cleanEndTypeDate = function () {
        $scope.dates.endType = null;
    };

    $scope.todayStartCallDate = function () {
        $scope.dates.startCall = new Date();
    };

    $scope.todayFinalCallDate = function () {
        $scope.dates.endCall = new Date();
    };

    $scope.cleanStartCallDate = function () {
        $scope.dates.startCall = null;
    };

    $scope.cleanEndCallDate = function () {
        $scope.dates.endCall = null;
    };

    $scope.cleanStartTypeDate();
    $scope.cleanEndTypeDate();
    $scope.cleanStartCallDate();
    $scope.cleanEndCallDate();

    $scope.openStartTypeDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.startTypeDateOpened = true;
    };

    $scope.openFinalTypeDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.finalTypeDateOpened = true;
    };

    $scope.openStartCallDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.startCallDateOpened = true;
    };

    $scope.openFinalCallDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.finalCallDateOpened = true;
    };

    $scope.format = 'dd/MM/yyyy';


});