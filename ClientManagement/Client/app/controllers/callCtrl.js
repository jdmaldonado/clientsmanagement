﻿CarrazoApp.controller('callCtrl', function callCtrl($scope, $filter, $rootScope, $routeParams, clients_Factory, cities_Factory, departments_Factory, countries_Factory, occupations_Factory, $timeout) {

    /* Este controlador debe recibir el código del Operador para que pueda funcionar correctamente */
    $scope.operatorCode = $routeParams.operatorCode;
    var countSystem;

    getCities = function () {
        $scope.ciudades = cities_Factory.city.GetAllCities({});
    }

    getFactories = function () {
        $scope.empresas = clients_Factory.getFactories.GetFactories({});
    }

    getDepartments = function () {
        $scope.departamentos = departments_Factory.department.GetAllDepartments({});
    }

    getCountries = function () {
        $scope.paises = countries_Factory.country.GetAllCountries({});
    }

    getOccupations = function () {
        $scope.ocupaciones = occupations_Factory.occupations.GetOccupations({});
    }

    getCities();
    getFactories();
    getDepartments();
    getCountries();
    getOccupations();

    function evaluateCallStatus(client) {

        if (client.estado) {
            //Solo se permite realizar la llamada fue realizada pero no fué exitosa
            $scope.calledClient = (client.estadoLlamada === 1) ? false : true;
            $scope.showTime = (client.estadoLlamada === 1) ? true : false;
        }
    }

    function showForm() {
        $scope.showForm = true;
        $scope.showFilter = false;
        $scope.showClientsTable = false;
        startCounter();
    }

    function showFilter() {
        $scope.showForm = false;
        $scope.showFilter = true;
        $scope.showClientsTable = false;
        $scope.filter = {
            type: 'Cedula'
        };
        clearCounter();
    }

    function showClientsTable() {
        $scope.showForm = false;
        $scope.showFilter = true;
        $scope.showClientsTable = true;
    }
    
    function startCounter() {
        countSystem = setInterval(function () {
            $scope.callSeconds = $scope.callSeconds + 1000;
            $scope.$apply();
        }, 1000);

        $scope.showTime = true;
    }

    function clearCounter() {
        $scope.callSeconds = 0;
        clearInterval(countSystem);
        $scope.showTime = false;
    }

    function resetCallValues() {
        clearCounter();
        $scope.formClient = null;
        $scope.calledClient = false;
        $scope.noRegistredClient = false;
    }

    function fixClientGenre(client) {
        client.genero = $filter('lowercase')(client.genero);
        return client;
    }

    $scope.loadDefaultData = function () {
        showFilter();
        $scope.callSeconds = 0;
        $scope.calledClient = false;
        $scope.noRegistredClient = false; /* Mensaje de Cliente no Registrado*/
        $scope.formClient = {}; /* Formulario Básico */
    }

    $scope.loadClient = function(client) {
        showForm();
        $scope.formClient = fixClientGenre(client);
        $scope.ciudades.map(function (objeto) {
            if (objeto.idCiudad === $scope.formClient.idCiudad) {
                $scope.city = objeto;
            }
        })

        $scope.departamentos.map(function (objeto) {
            if (objeto.idDepartamento === $scope.formClient.idDepartamento) {
                $scope.department = objeto;
            }
        })

        $scope.ocupaciones.map(function (objeto) {
            if (objeto.idOcupacion == $scope.formClient.idOcupacion) {
                $scope.occupation = objeto;
            }
        })

        $scope.paises.map(function (objeto) {
            if (objeto.idPais === $scope.formClient.idPais) {
                $scope.country = objeto;
            }
        })

        $scope.empresas.map(function (objeto) {
            if (objeto.idEmpresa === $scope.formClient.idEmpresa) {
                $scope.factory = objeto;
            }
        })

        evaluateCallStatus($scope.formClient);       
    }
        
    $scope.Load = function (identification) {

        resetCallValues();
        
        clients_Factory.client.GetByNumberAndType({ number: $scope.filter.number, type: $scope.filter.type }).$promise.then(function (data) {

            if (data.length > 0) {
                if (data.length > 1) {
                    $scope.clients = data;
                    showClientsTable();
                } else {
                    //Si sólo retornó un Cliente, Carguelo en el formulario
                    $scope.loadClient(data[0]);
                }
            }
            else {
                $scope.noRegistredClient = true;
            }
        });
    }

    $scope.loadDefaultData();

    /* Establece como "llamado" al cliente seleccionado */
    $scope.Call = function () {
        
        $scope.formClient.duracionLlamada = $scope.callSeconds;
        $scope.formClient.idEmpresa = $scope.factory.idEmpresa;
        $scope.formClient.idOcupacion = ($scope.occupation !== undefined)? $scope.occupation.idOcupacion: null;
        clients_Factory.callClient.call({ operatorCode: $scope.operatorCode }, $scope.formClient);

        clearCounter();
        $scope.loadDefaultData();

    }

    /* DATEPICKER  */
    $scope.today = function () {
        $scope.formClient.fechaNacimiento = new Date();
    };

    $scope.cleanDate = function () {
        $scope.formClient.fechaNacimiento = null;
    };    

    $scope.cleanDate();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.bornDateOpened = true;
    };

    $scope.format = 'dd/MM/yyyy';

    
});

