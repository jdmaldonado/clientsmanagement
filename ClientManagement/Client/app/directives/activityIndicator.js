﻿CarrazoApp.directive('activityIndicator', function () {
    return function (scope, element, attrs) {
        var presets = {
            "tiny": { segments: 12, width: 2, space: 1, length: 3, color: '#030303', speed: 1.5 },
            "standard": {},
            "large": { segments: 12, width: 5.5, space: 6, length: 13, color: '#252525', speed: 1.5 }
        };
        var opts;
        if (attrs.indicatorType && attrs.indicatorType in presets) {
            opts = presets[attrs.indicatorType];
        } else {
            opts = presets["standard"];
        }

        scope.$watch(attrs.activityIndicator, function (value) {
            if (value) {
                element.activity(opts);
            } else {
                element.activity(false);
            }
        });
    };
});