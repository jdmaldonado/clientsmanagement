﻿CarrazoApp.factory('clients_Factory', ['$resource', function ($resource) {

    return {
        clients:
            $resource('Clients', { },
                {
                    "GetClients": { method: "GET", params: { }, isArray: true }
                }
            ),
        client:
            $resource('Clients/:number/:type', { number: '@number', type: '@type' },
                {
                    "GetByNumberAndType": { method: "GET", params: { number: '@number', type: '@type' }, isArray: true }
                }
            ),
        clientCall:
            $resource('Clients/CallStatus/:identification', { identification: '@identification' },
                {
                    "GetCallStatusByIdentification": { method: "GET", params: { identification: '@identification' }, isArray: true }
                }
            ),
        clientsGrid:
            $resource('Clients/Historical', {}),

        getFactories:
            $resource('Clients/Factories', {},
                {
                    "GetFactories": { method: "GET", params: {}, isArray: true }
                }
            ),

        createClient:
            $resource('Clients/Create/:operatorCode', { operatorCode: '@operatorCode' },
                {
                    "PostClient": { method: "POST", params: { operatorCode: '@operatorCode'}, isArray: false }
                }
            ),
        updateClient:
             $resource('Clients/update/:idClient/:operatorCode', { idClient: '@idClient', operatorCode: '@operatorCode' },
                {
                    "putClient": { method: "PUT", params: { idClient: '@idClient', operatorCode: '@operatorCode' }, isArray: false }
                }
            ),
        callClient:
             $resource('Clients/update/call/:operatorCode', { operatorCode: '@operatorCode' },
                {
                    "call": { method: "PUT", params: { operatorCode: '@operatorCode' }, isArray: false }
                }
            ),
        updateStatus:
            $resource('Clients/update/CallStatus/:operatorCode', { operatorCode: '@operatorCode' },
                {
                    "callStatus": { method: "PUT", params: { operatorCode: '@operatorCode' }, isArray: false }
                }
            ),
        factory: 
            $resource('Clients/factories', {},
                {
                    "GetFactories": { method: "GET", params: {}, isArray: true }
                }
            ),
    };

}]);