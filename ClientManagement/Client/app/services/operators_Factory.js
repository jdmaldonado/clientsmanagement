﻿CarrazoApp.factory('operators_Factory', ['$resource', function ($resource) {

    return {
        operators:
            $resource('Operators', {},
                {
                    "GetOperators": { method: "GET", params: {}, isArray: true }
                }
            ),
        updateOperator:
             $resource('Operators/update/:idOperador', { idOperador: '@idOperador' },
                {
                    "putOperator": { method: "PUT", params: { idOperador: '@idOperador' }, isArray: false }
                }
            ),
        createOperator:
             $resource('Operators/create', {},
                {
                    "postOperator": { method: "POST", params: {}, isArray: false }
                }
            )
    };

}]);