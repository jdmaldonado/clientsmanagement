﻿CarrazoApp.factory('occupations_Factory', ['$resource', function ($resource) {

    return {
        occupations:
            $resource('Occupations', {},
                {
                    "GetOccupations": { method: "GET", params: {}, isArray: true }
                }
            ),
        updateOccupation:
             $resource('Occupations/update/:idOcupacion', { idOcupacion: '@idOcupacion' },
                {
                    "putOccupation": { method: "PUT", params: { idOcupacion: '@idOcupacion' }, isArray: false }
                }
            ),
        createOccupation:
             $resource('Occupations/create', {},
                {
                    "postOccupation": { method: "POST", params: {}, isArray: false }
                }
            )
    };

}]);