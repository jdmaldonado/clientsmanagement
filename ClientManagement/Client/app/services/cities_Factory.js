﻿CarrazoApp.factory('cities_Factory', ['$resource', function ($resource) {

    return {
        city:
            $resource('Ciudades', {},
                {
                    "GetAllCities": { method: "GET", params: { }, isArray: true }
                }
            )
        //roles:
        //     $resource('Menus/Roles/:idPerfil', { idPerfil: '@idPerfil' },
        //        {
        //            "GetRolesByIdPerfil": { method: "GET", params: { idPerfil: '@idPerfil' }, isArray: true }
        //        }
        //     ),
        //addRol:
        //     $resource('Menus/Roles/:idPerfil/:idRol', { idPerfil: '@idPerfil', idRol: '@idRol' },
        //        {
        //            "AddRolToPerfil": { method: "POST", params: { idPerfil: '@idPerfil', idRol: '@idRol' }, isArray: true }
        //        }
        //     ),

        //deleteRol:
        //    $resource('Menus/Roles/:idPerfil/:idRol', { idPerfil: '@idPerfil', idRol: '@idRol' },
        //       {
        //           "DeleteRolToPerfil": { method: "DELETE", params: { idPerfil: '@idPerfil', idRol: '@idRol' }, isArray: true }
        //       }
        //    )
    };

}]);