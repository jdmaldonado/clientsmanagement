﻿CarrazoApp.factory('authInterceptor_factory', ['toastr', 'notification_Factory', '$window', '$q', '$location', '$rootScope', function (toastr, notification_Factory, $window, $q, $location, $rootScope) {
    return {

        'response': function (response) {
            if (response.config.method == 'PUT' && response.status == 200) {
                notification_Factory.success.putSuccess();
            } else if (response.config.method == 'POST' && (response.status == 200 || response.status == 201)) {
                notification_Factory.success.postSuccess();
            } else if (response.config.method == 'DELETE' && response.status == 200) {
                notification_Factory.success.deleteSuccess();
            }
            return response;
        },
        'responseError': function (rejection) {
            //if (rejection.status === 401) {
            //    $window.location.href = 'http://www.lapflotas.com';
            //} else 
            if (rejection.status === 500) {
                notification_Factory.errors.serverError();
            } else if (rejection.config.method === 'PUT' && rejection.status === 400) {
                notification_Factory.errors.putError();
            } else if (rejection.config.method === 'POST' && rejection.status === 400) {
                notification_Factory.errors.postError();
            }

            return $q.reject(rejection);
        }
    };
}]);