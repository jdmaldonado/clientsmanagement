﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.ModelBinding;
using ClientManagement.DataBase;
using ClientManagement.Models;
using ClientManagement.Services;

namespace ClientManagement.Modules
{
    public class OccupationsModule : NancyModule
    {
        public OccupationsModule()
            : base("Occupations")
        {
            Get["/"] = parameters =>
            {
                return DbManager.GetAllOccupations();
            };

            Put["/update/{idOcupacion}"] = parameters =>
            {
                var _occupation = this.Bind<Occupation>();
                var editedOccupation = DbManager.UpdateOccupation(_occupation);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Post["/create"] = parameters =>
            {
                var _occupation = this.Bind<Occupation>();

                Operator createdOccupation = DbManager.CreateOccupation(_occupation);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };
        }
    }
}