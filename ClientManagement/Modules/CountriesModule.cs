﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using ClientManagement.DataBase;

namespace ClientManagement.Modules
{
    public class CountriesModule : NancyModule
    {
        public CountriesModule()
            : base("Paises")
        {
            Get["/"] = parameters =>
            {                
                var countries = DbManager.GetAllCountries();

                return countries;
            };

            //Get["/Roles/{IdPerfil}"] = parameters =>
            //{
            //    var idProfile = (int)parameters.IdPerfil;
            //    var roles = DbManager.GetRolesByIdProfile(idProfile);

            //    return roles;
            //};

            //Post["/Roles/{IdPerfil}/{IdRol}"] = parameters =>
            //{
            //    var idPerfil = (int)parameters.IdPerfil;
            //    var idRol = (int)parameters.IdRol;
            //    var roles = DbManager.AddRoleToProfile(idPerfil, idRol);

            //    return roles;
            //};

            //Delete["/Roles/{IdPerfil}/{IdRol}"] = parameters =>
            //{
            //    var idPerfil = (int)parameters.IdPerfil;
            //    var idRol = (int)parameters.IdRol;
            //    var roles = DbManager.QuitRoleToProfile(idPerfil, idRol);

            //    return new Response().WithStatusCode(HttpStatusCode.OK);
            //};
        }
    }
}