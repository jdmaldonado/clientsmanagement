﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.ModelBinding;
using ClientManagement.DataBase;
using ClientManagement.Models;
using ClientManagement.Services;
using System.Globalization;
using Nancy.Responses;

namespace ClientManagement.Modules
{
    public class ClientsModule : NancyModule
    {
        public ClientsModule( ClientService service)
            : base("Clients")
        {
            Get["/"] = parameters =>
            {
                List<Client> clients = service.SetOperatorNameForGet();

                return clients;
            };

            Get["/Factories"] = parameters =>
            {
                List<Factory> factories = DbManager.GetAllFactories();

                return factories;
            };

            Get["/CallStatus/{Identification}"] = parameters =>
            {
                var clientIdentification = (long)parameters.Identification;

                List<Client> _clients = new List<Client>();

                Client _client = DbManager.GetCallStatusByClientId(clientIdentification);

                if (_client == null)
                {
                    return _clients;
                }
                else
                {
                    _clients.Add(_client);

                    return _clients;
                }
            };

            Get["/{Number}/{Type}"] = parameters =>
            {
                var numberFilter    = (long)parameters.Number;
                var typeFilter      = (string)parameters.Type;
                
                var clients = DbManager.GetClientsByNumberAndType(numberFilter, typeFilter);

                if (clients.Any())
                {
                    foreach (var client in clients)
                    {
                        var _operator = client.LlamadoPor == null ? null : DbManager.GetOperatorFromId(client.LlamadoPor.Value);
                        client.Asesor = _operator == null ? null : _operator.Nombres + " " + _operator.Apellidos;
                    }
                }

                return clients;
            };

            Get["/Historical"] = parameters =>
            {
                var culture = new CultureInfo("es-CO");

                var format      = (Request.Query.format     == null) ? null : Request.Query.format;

                var idEmpresa   = (Request.Query.idEmpresa  == null) ? null : Request.Query.idEmpresa;
                var callStatus  = (Request.Query.callStatus == null) ? null : Request.Query.callStatus;
                var status      = (Request.Query.status     == null) ? null : Request.Query.status;
                var startType   = (Request.Query.startType  == null) ? null : (new DateTime(1970, 1, 1)).AddMilliseconds(double.Parse(Request.Query.startType));
                var endType     = (Request.Query.endType    == null) ? null : (new DateTime(1970, 1, 1)).AddMilliseconds(double.Parse(Request.Query.endType));
                var startCall   = (Request.Query.startCall  == null) ? null : (new DateTime(1970, 1, 1)).AddMilliseconds(double.Parse(Request.Query.startCall));
                var endCall     = (Request.Query.endCall    == null) ? null : (new DateTime(1970, 1, 1)).AddMilliseconds(double.Parse(Request.Query.endCall));
                var page        = Request.Query.page;
                var export      = (!string.IsNullOrEmpty(format) && format.ToString().Equals("spreadsheet"));

                var clients     = service.SetNamesForGrid(DbManager.GetClientsForGrid(startType, endType, startCall, endCall, status, idEmpresa, callStatus, page, export));

                if (string.IsNullOrEmpty(format) || !format.ToString().Equals("spreadsheet")) return clients;
                
                var file = service.GenerateExcelFileForClients(clients, culture);
                var response = new StreamResponse(() => file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                return response.AsAttachment("HistoricClients.xlsx");
            };

            Post["/create/{operatorCode}"] = parameters =>
            {
                var operatorCode = (long)parameters.operatorCode;
                var client = this.Bind<Client>();

                Client createdClient = service.SetOperatorIdForCreation(client, operatorCode);

                return createdClient;
            };

            Put["/update/{IdClient}/{operatorCode}"] = parameters =>
            {
                var operatorCode = (long)parameters.operatorCode;
                var client = this.Bind<Client>();

                var status = service.SetOperatorIdForUpdate(client, operatorCode);


                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Put["/update/call/{operatorCode}"] = parameters =>
            {
                var operatorCode = (long)parameters.operatorCode;
                var client = this.Bind<Client>();

                var status = service.SetOperatorIdForCall(client, operatorCode);


                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Put["/update/CallStatus/{operatorCode}"] = parameters =>
            {
                var operatorCode = (long)parameters.operatorCode;
                var client = this.Bind<Client>();

                var status = service.SetOperatorIdForCallStatus(client, operatorCode);


                return new Response().WithStatusCode(HttpStatusCode.OK);
            };
        }
    }
}