﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using ClientManagement.Models;

namespace ClientManagement.DataBase
{
    public class DbManager
    {
        /*POST*/
        public static dynamic AddRoleToProfile(int idPerfil, int idRol)
        {
            var db = Database.OpenNamedConnection("CLIENTS");

            dynamic record = new SimpleRecord();
            record.IdPerfil = idPerfil;
            record.IdRol = idRol;

            return db.PerfilesRoles.Insert(record);
        }

        public static dynamic CreateClient(Client client)
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Clientes.Insert(client);
        }

        public static dynamic CreateOperator(Operator _operator)
        {
            var db = Database.OpenNamedConnection("CLIENTS");

            dynamic record = new SimpleRecord();
            record.Codigo = _operator.Codigo;
            record.Nombres = _operator.Nombres;
            record.Apellidos = _operator.Apellidos;
            record.IdPerfil = _operator.IdPerfil;

            return db.Operadores.Insert(record);
        }

        public static dynamic CreateOccupation(Occupation _ocupation)
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Ocupaciones.Insert(_ocupation);
        }

        public static dynamic CreateProfile(Profile profile)
        {
            var db = Database.OpenNamedConnection("CLIENTS");

            dynamic record = new SimpleRecord();
            record.Perfil = profile.Perfil;

            return db.Perfiles.Insert(record);
        }


        /*GET*/
        public static List<Factory> GetAllFactories()
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Empresas.All();
        }

        public static List<City> GetAllCities()
        {
            var db = Database.OpenNamedConnection("CLIENTS");

            return db.Ciudades.All()
                                .OrderBy(db.Ciudades.Descripcion);
        }

        public static City GetCityById(int id)
        {
            var db = Database.OpenNamedConnection("CLIENTS");

            return db.Ciudades.All()
                .Where(db.Ciudades.IdCiudad == id).FirstOrDefault();
        }

        public static List<Country> GetAllCountries()
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Paises.All()
                                .OrderBy(db.Paises.Descripcion);
        }

        public static List<Occupation> GetAllOccupations()
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Ocupaciones.All();
        }

        public static Country GetCountryById(int id)
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Paises.All()
                .Where(db.Paises.IdPais == id).FirstOrDefault();
        }

        public static List<Department> GetAllDepartments()
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Departamentos.All()
                                .OrderBy(db.Departamentos.Descripcion);
        }

        internal static Occupation GetOccupationById(int idOcupacion)
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Ocupaciones.All()
                .Where(db.Ocupaciones.IdOcupacion == idOcupacion).FirstOrDefault();
        }

        public static Department GetDepartmentById(int id)
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Departamentos.All()
                .Where(db.Departamentos.IdDepartamento == id).FirstOrDefault();
        }

        public static Client GetCallStatusByClientId(long identification)
        {
            var db = Database.OpenNamedConnection("CLIENTS");

            return db.Clientes.All()
                .Select(
                    db.Clientes.IdCliente,
                    db.Clientes.EstadoLlamada.As("EstadoActualLlamada"),
                    db.Clientes.Estado
                )
                .Where(db.Clientes.Cedula == identification).FirstOrDefault();
        }

        public static List<Client> GetClientsByNumberAndType(long number, string type)
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Clientes.All()
                .Where(db.Clientes[type] == number);
        }

        public static List<Client> GetClients()
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Clientes.All();
        }

        public static List<Client> GetClientsForGrid(DateTime? initialTypeDate, DateTime? finalTypeDate, 
            DateTime? initialCallDate, DateTime? finalCallDate,
            bool? status, int? idEmpresa, int? callStatus,
            int page, bool export)
        {
            try
            {
                var db = Database.OpenNamedConnection("CLIENTS");
                var initFormat = "yyyy-dd-MM 00:00:00";
                var finalFormat = "yyyy-dd-MM 23:59:59";

                var creationInit = initialTypeDate?.ToString(initFormat);
                var creationEnd = finalTypeDate?.ToString(finalFormat);

                var callInit = initialCallDate?.ToString(initFormat);
                var callEnd = finalCallDate?.ToString(finalFormat);

                //[dbo].[sp_GetClients]
                //    NULL,--@CreationInit
                //    NULL, --@CreationEn
                //    NULL, --@CallInit
                //    NULL, --@CallEnd
                //    NULL, --@ClientStatus
                //    NULL, --@IdFactory
                //    NULL, --@CallStatus
                //    1,    --@Pag
                //    NULL  --@Export

                return db.sp_GetClients(creationInit, creationEnd, callInit, callEnd, status, idEmpresa, callStatus, page, export);
            }
            catch (Exception ex)
            {
                Logger.LogException(new Exception($"Error: GetClientsForGrid... {ex.Message}"));
                return null;
            }
            
        }

        public static List<Operator> GetOperators()
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Operadores.All();
        }

        public static Operator GetOperatorFromCode(long operatorCode)
        {
            var db = Database.OpenNamedConnection("CLIENTS");

            return db.Operadores.All()
                .Where(db.Operadores.Codigo == operatorCode).FirstOrDefault();

        }

        public static Operator GetOperatorFromId(int? idOperator)
        {
            var db = Database.OpenNamedConnection("CLIENTS");

            return db.Operadores.All()
                .Where(db.Operadores.IdOperador == idOperator).FirstOrDefault();
        }

        public static List<Menu> GetMenus(long operatorCode)
        {
            var db = Database.OpenNamedConnection("CLIENTS");

            return db.Operadores
           .Select(
               db.Operadores.IdOperador,
               db.Operadores.Nombres,
               db.Operadores.Codigo,
               db.Roles.Rol,
               db.Roles.Icono,
               db.Roles.Href
           )
           .Join(db.PerfilesRoles).On(db.Operadores.IdPerfil == db.PerfilesRoles.IdPerfil)
           .Join(db.Roles).On(db.PerfilesRoles.IdRol == db.Roles.IdRol)
           .Where(db.Operadores.Codigo == operatorCode)
           .OrderBy(db.Roles.Rol);
        }

        public static List<Profile> GetProfiles()
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Perfiles.All();
        }

        public static List<Role> GetRolesByIdProfile(int idPerfil)
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.sp_GetRolesByIdProfile(idPerfil);
        }


        /*PUT*/
        public static dynamic ChangeCallStatus(Client client)
        {
            var db = Database.OpenNamedConnection("CLIENTS");

            dynamic record = new SimpleRecord();
            record.IdCliente = client.IdCliente;
            record.EstadoLlamada = client.EstadoLlamada;
            record.FechaModificacion = DateTime.Now;

            return db.Clientes.UpdateByIdCliente(record);
        }

        public static dynamic SetClientToCalled(Client client)
        {
            var db = Database.OpenNamedConnection("CLIENTS");

            dynamic record = new SimpleRecord();
            record.IdCliente = client.IdCliente;
            record.LlamadoPor = client.LlamadoPor;
            record.FechaLlamada = client.FechaLlamada;
            record.Estado = client.Estado;
            record.DuracionLlamada = client.DuracionLlamada;
            record.EstadoLlamada = client.EstadoLlamada;
            record.FechaModificacion = DateTime.Now;

            return db.Clientes.UpdateByIdCliente(client);
        }

        public static dynamic UpdateClient(Client client)
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            client.FechaModificacion = DateTime.Now;
            return db.Clientes.UpdateByIdCliente(client);
        }

        public static dynamic UpdateOccupation(Occupation _occupation)
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.Ocupaciones.UpdateByIdOcupacion(_occupation);

        }

        public static dynamic UpdateOperator(Operator _operator)
        {
            var db = Database.OpenNamedConnection("CLIENTS");

            dynamic record = new SimpleRecord();
            record.IdOperador = _operator.IdOperador;
            record.Codigo = _operator.Codigo;
            record.Nombres = _operator.Nombres;
            record.Apellidos = _operator.Apellidos;
            record.IdPerfil = _operator.IdPerfil;

            return db.Operadores.UpdateByIdOperador(record);
        }

        public static int UpdateProfile(Profile profile)
        {
            var db = Database.OpenNamedConnection("CLIENTS");

            dynamic record = new SimpleRecord();
            record.IdPerfil = profile.IdPerfil;
            record.Perfil = profile.Perfil;

            return db.Perfiles.UpdateByIdPerfil(record);
        }


        /*DELETE*/
        public static dynamic QuitRoleToProfile(int idPerfil, int idRol)
        {
            var db = Database.OpenNamedConnection("CLIENTS");
            return db.PerfilesRoles.Delete(idPerfil: idPerfil, idRol: idRol);
        }

    }
}